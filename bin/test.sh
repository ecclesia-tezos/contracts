#!/bin/bash
smart_contract=$1
test_output_directory=/test
container_test_command="/smartpy/SmartPyBasic/SmartPy.sh test /contracts/$smart_contract $test_output_directory/$smart_contract && cat $test_output_directory/$smart_contract/test.output"

docker run --rm -v "$(pwd)"/contracts:/contracts development-environment /bin/bash -c "$container_test_command"
