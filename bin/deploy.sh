#!/bin/bash
set -e

contract_deploy_command="cd /contract_deployer && node deploy_contract.js"

docker run --rm -it -v "$(pwd)/build":/build -v "$(pwd)/contract_deployer":/contract_deployer development-environment /bin/bash -c "$contract_deploy_command"
