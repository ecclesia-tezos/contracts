#!/bin/bash
set -e

smart_contract=$1
class_call=$2
contract_compile_command="/smartpy/SmartPyBasic/SmartPy.sh compile /contracts/$smart_contract \"$class_call\" /build/"

docker run --rm -v "$(pwd)"/contracts:/contracts -v "$(pwd)"/build:/build development-environment /bin/bash -c "$contract_compile_command"
echo "Built $smart_contract artifact successfully in $(pwd)/build/"
