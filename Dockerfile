FROM ubuntu:18.04

# Install SmartPy and NodeJS
RUN apt-get update && apt-get install --yes python3 curl
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get update && apt-get install --yes nodejs libudev-dev libusb-1.0-0-dev build-essential

# Install SmartPy for compilation and testing
COPY lib/smartpy.sh /bin/smartpy
RUN smartpy local-install /smartpy

# Install deployment tools
COPY contract_deployer /contract_deployer
WORKDIR /contract_deployer/
RUN npm install

# Import secrets needed for Babylonnet interaction and conseil API 
COPY .secrets /secrets

WORKDIR /
