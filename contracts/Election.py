import smartpy as sp

class Election(sp.Contract):
    def __init__(self, candidates_list, modulus):
        zeroes = [0] * len(candidates_list)
        votes = dict(zip(candidates_list, zeroes))
        venues = dict()
        self.init(
            votes = sp.map(l = votes, tkey = sp.TString, tvalue = sp.TInt),
            venues = sp.map(l = venues, tkey = sp.TString, tvalue = sp.TInt),
            modulus = sp.string(modulus)
        )

    @sp.entry_point
    def vote_for(self, params):
        sp.verify(self.data.votes.contains(params.candidate))
        self.data.votes[params.candidate] += 1
        sp.if self.data.venues.contains(params.venue):
            self.data.venues[params.venue] += 1
        sp.else:
            self.data.venues[params.venue] = 1

@sp.add_test(name = "Election Test")
def init_test():
    modulus = 'some_modulus_here'
    scenario = sp.test_scenario()
    election = Election(['Alice', 'Bob'], modulus)
    scenario += election
    scenario.verify(election.data.votes['Alice'] == 0)
    scenario.verify(election.data.votes['Bob'] == 0)
    scenario.verify(election.data.modulus == modulus)

    scenario += election.vote_for(candidate = 'Bob', venue = 'University of Paris')
    scenario += election.vote_for(candidate = 'Alice', venue = 'University of Paris')
    scenario += election.vote_for(candidate = 'Alice', venue = 'University of Manhattan')

    scenario.verify(election.data.votes['Bob'] == 1)
    scenario.verify(election.data.votes['Alice'] == 2)
    scenario.verify(election.data.venues['University of Paris'] == 2)
    scenario.verify(election.data.venues['University of Manhattan'] == 1)

    scenario += election.vote_for(candidate = 'Eve', venue = 'University of Manhattan').run(valid = False)
