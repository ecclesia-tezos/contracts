const conseiljs = require('conseiljs');
const fs = require('fs');

function readFileString(filePath) {
  return fs.readFileSync(filePath).toString();
}

function readJSONConfig(filePath) {
  return JSON.parse(readFileString(filePath));
}

const tezosNode = 'https://tezos-dev.cryptonomic-infra.tech';

const conseilServer = readJSONConfig('/secrets/conseil_server_config.json');

const keystore = readJSONConfig('/secrets/test_key.json');

const contract = readFileString('/build/contractCode.tz.json');

const storage = '{"prim":"Pair","args":[{"prim":"Pair","args":[{"string":"bbca88c6f36ca2f62736ac785f5ca33214ade26977d254eef1c9eda48f64e5951774194136ffaac67666de1699eb785cc4fcae04577fa021bf324eaaa8dbc0efd2c83e09947c35f319318ebdd9e3a15175770155afe17ba853b070f590c758263baedf190c501c6794c09a7f23a79e93e3e909315f0b822b41ad0c4cdea0bd5d83d2a561f390168193aaf344dcf961ee3a1983d81e202e5ac6b1bf8ef46626b94e33812e689bdc3bb25ceb2d3b253f40778be02ec493f1e452f135d72fa28ee9ae18bf3e61255adfc3036577dc36a50a5a240433a5a4bbaf6fdfb8d8721ba861350cd023d7b8a015c956d0c670a95b1744be85c500e320b31a38ef2d36cf9f8a89e09cca67b01d572f598dde279eaf3b7dc519811ec2edef935b876589923469ca21ccd179b0b7d1c0a0e2be0f7bb67afab7725dfecac0cdeb0e261f4eae393837bc412381ee50af5927492d6c15a0ea8a6173ee5397e49a9ca476c1052a2df8b6010466b2bd3f11c0f876527cfa083eb3da436d29e9b09c0198490a2d893105"},[]]},[{"prim":"Elt","args":[{"string":"Alice"},{"int":"0"}]},{"prim":"Elt","args":[{"string""Bob"},{"int":"0"}]}]]}';

async function deployContract() {
  const nodeResult = await conseiljs.TezosNodeWriter.sendContractOriginationOperation(
    tezosNode,
    keystore,
    0,
    undefined,
    100000,
    '',
    1000,
    100000,
    contract,
    storage,
    conseiljs.TezosParameterFormat.Micheline
  );

  const groupid = nodeResult['operationGroupID'].replace(/\"/g, '').replace(/\n/, '');
  console.log(`Injected operation group id ${groupid}`);

  const conseilResult = await conseiljs.TezosConseilClient.awaitOperationConfirmation(
    conseilServer,
    conseilServer.network,
    groupid,
    5
  );
  console.log(`Originated contract at ${conseilResult[0].originated_contracts}`);
}

deployContract();
