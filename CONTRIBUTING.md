# Developer Guide
## Developing
### Docker
To build the project, you need a working docker installation. Docker needs to be callable without super user permissions for the scripts to work out of the box. See how to install Docker [here](https://docs.docker.com/install/).

### Building
After pulling the repository, cd into it and run
`bin/build.sh`

This step will create a docker image called development-environment, which is used by other scripts
### Contract compilation
To compile a contract execute
`bin/compile.sh <contract-file> <contract-call>`
Example:
`bin/compile.sh Election.py "Election(['Alice','Bob'])"`

This step will create a build/ directory and output the compiled Micheline and Michelson of the contracts there.
**NB: Only the name of the contract file is needed, the script is set up to look into the contracts directory by default**
### Contract testing
To test a contract execute
`bin/test.sh <contract-file>`
This will run the tests in the contract and output the results.

### Contract deployment to test network
To deploy a contract execute
`bin/deploy.sh` 
After you have run the build command. This will deploy the contract to the test network using the ConseilJS API.
**NB: Currently the storage of the contract will always be initialised as ["Alice", "Bob"], because SmartPy's compilation doesn't generate storage as JSON and the ConseilJS API cannot take Michelson for the storage parameter. More work to automate this is TBD**
**NB: To use this, some configuration is required. Read below.** 

### Configuration for deployment
You only need to do this if you want to use the deploy script. The script expects a `.secrets` directory to be present in your project folder with two items in it:
`.secrets/conseil_server_config.json`
{
	  "url":"https://conseil-dev.cryptonomic-infra.tech",
	  "apiKey":\<INCLUDE API KEY HERE\>,
	  "network":"babylonnet"
}
To get an API key from ConseilJS, go to [https://nautilus.cloud/home](https://nautilus.cloud/home) and copy the Dev key from there.

`.secrets/test_key.json`
{
	  "publicKey":"\<INCLUDE PUBLIC KEY HERE\>",
	  "privateKey":"\<INCLUDE PRIVATE KEY HERE\>",
	  "publicKeyHash":"\<INCLUDE KEY HASH HERE\>",
	  "seed":"",
	  "storeType":1
}
To get the keys to a Faucet test net account, go to [https://faucet.tzalpha.net/](https://faucet.tzalpha.net/) and get an account and copy its JSON. Then go to [https://smartpy.io/demo/faucetImporter.html](https://smartpy.io/demo/faucetImporter.html) and paste the JSON and follow the steps.
